import os

from flask import Flask

DB = 'rsvp.sqlite'


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="SomethingSecret",
        DATABASE=os.path.join(app.instance_path, DB)
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import auth, info, music
    app.register_blueprint(auth.bp)
    app.register_blueprint(info.bp)
    app.register_blueprint(music.bp)

    app.add_url_rule('/', 'welcome', info.welcome)

    from . import db
    db.init_app(app)

    return app


app = create_app()

if __name__ == '__main__':
    app = create_app()