from flask import (
    Blueprint, render_template, request
)

from RSVP.db import addSong, getSongList

bp = Blueprint('music', __name__)


@bp.route('/music', methods=('GET', 'POST'))
def songrequest():
    error = 0
    song_list = []
    if request.method == 'POST':
        song_title = request.form['title']
        artist = request.form['artist']
        addSong(song_title, artist)
    else:
        song_list = getSongList()
    return render_template('music.jinja', song_list=song_list, error=error)


@bp.route('/songlist', methods=('GET', 'POST'))
def songlist():
    error = 0
    song_list = []
    songs = getSongList()
    for row in songs:
        song_list.append((row[0], row[1]))

    return render_template('dashboard/songlist.jinja',
                           song_list=song_list, error=error)
