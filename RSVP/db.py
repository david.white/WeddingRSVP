import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext
from werkzeug.security import generate_password_hash

DB = 'rsvp.db'


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(load_test_data)
    app.cli.add_command(get_guestlist_command)
    app.cli.add_command(createadmin)


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(current_app.config['DATABASE'],
                               detect_types=sqlite3.PARSE_DECLTYPES)
        g.db.row_factory = sqlite3.Row
    return g.db


def init_db():
    """Clear the existing data and create new tables."""
    db = get_db()
    with current_app.open_resource('schema.sql', mode='r') as f:
        db.executescript(f.read())


def addGuest(name, attending=0, diet_restriction=None):
    db = get_db()
    cursor = db.cursor()
    cursor.execute("REPLACE INTO guests VALUES(?,?,?,date('now'))",
                   (name.strip(), attending, diet_restriction))
    db.commit()


def addAdmin(username, password):
    db = get_db()
    cursor = db.cursor()
    hashed_pw = generate_password_hash(password)
    cursor.execute("INSERT INTO admins VALUES(?,?)",
                   (username, hashed_pw))
    db.commit()


def getGuestList():
    db = get_db()
    cursor = db.cursor()
    cursor.execute('SELECT * FROM guests')
    return cursor


def addSong(title, artist):
    db = get_db()
    cursor = db.cursor()
    cursor.execute('INSERT INTO songs VALUES(?,?)',
                   (title, artist))
    db.commit()


def getSongList():
    db = get_db()
    cursor = db.cursor()
    cursor.execute('SELECT * FROM songs')
    return cursor


@click.command('show-guests')
@with_appcontext
def get_guestlist_command():
    print("Guest Name, Plus One Name, Attending")
    for e in getGuestList():
        print(str(e['guest_name']) + ' ' +
              str(e['plus_one_name']) + ' ' +
              str(e['attending']))


@click.command('db-test')
@with_appcontext
def load_test_data():
    addGuest('James Lounsbury', True)
    addGuest('Jeff Siegle',  True)
    addGuest('Kathryn Eagles', True)
    addGuest('Obi-Wan Kenobi', False)
    addAdmin('admin', 'password')


@click.command('init-db')
@with_appcontext
def init_db_command():
    init_db()
    click.echo('Initialized the database.')


@click.command('create-admin')
@click.argument('username', nargs=1)
@click.argument('password', nargs=1)
@with_appcontext
def createadmin(username, password):
    addAdmin(username, password)
    click.echo(f"Added Admin Account: {username}")


def close_db(e=None):
    db = g.pop('db', None)
    if db is not None:
        db.close()
