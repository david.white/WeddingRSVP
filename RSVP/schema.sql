DROP TABLE IF EXISTS guests;
DROP TABLE IF EXISTS admins; 
DROP TABLE IF EXISTS songs;

CREATE TABLE guests (
    guest_name TEXT NOT NULL PRIMARY KEY,
    attending INTEGER NOT NULL,
    diet_restrict TEXT,
    rsvp_date TEXT
);

CREATE TABLE admins (
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL
);

CREATE TABLE songs (
    title TEXT NOT NULL,
    artist TEXT
)