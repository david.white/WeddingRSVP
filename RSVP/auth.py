from flask import (
    Blueprint, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash

from RSVP.db import get_db, addGuest, getGuestList

bp = Blueprint('auth', __name__)


@bp.route('/rsvp', methods=('GET', 'POST'))
def rsvp():
    rsvpmap = {"Yes": 1, "No": 0}
    error = 0
    if request.method == 'POST':
        attending = rsvpmap[request.form['attending']]
        guestlist = request.form.getlist('name')
        diet_restriction = request.form.get('dietary')
        for name in guestlist:
            if name:
                addGuest(name, attending, diet_restriction)
    return render_template('rsvp.jinja', error=error)


@bp.route('/administration', methods=('GET', 'POST'))
def administration():
    if request.method == 'GET':
        error = None
        try:
            if not session['logged_in']:
                return redirect(url_for('auth.adminlogin'))
        except KeyError:
            return redirect(url_for('auth.adminlogin'))

        guestList = []
        for row in getGuestList():
            attending = row['attending']
            guestList.append((row['guest_name'],
                              "Yes" if attending else "No",
                              row['diet_restrict'],
                              row['rsvp_date']))

        return render_template('dashboard/dashboard.jinja',
                               guestList=guestList,
                               error=error)


@bp.route('/adminlogin', methods=('GET', 'POST'))
def adminlogin():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        user = db.execute(
            'SELECT * FROM admins WHERE username =?', (username,)
            ).fetchone()

        if user is None:
            error = 'Incorrect username.'
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password.'

        if error is None:
            session.clear()
            session['logged_in'] = True
            return redirect(url_for('auth.administration'))
    return render_template('login.jinja', error=error)


@bp.route('/adminlogout')
def adminlogout():
    session['logged_in'] = False
    return redirect(url_for('auth.rsvp'))
