from flask import (
    Blueprint, render_template
)

bp = Blueprint('info', __name__)


@bp.route('/welcome', methods=('GET',))
def welcome():
    error = 0
    return render_template('welcome.jinja', error=error)


@bp.route('/accomodations', methods=('GET',))
def accomodations():
    error = 0
    return render_template('Accomodations.jinja', error=error)


@bp.route('/contact', methods=('GET',))
def contact():
    return render_template('contact.jinja')


@bp.route('/registry', methods=('GET',))
def registry():
    return render_template('registry.jinja')


@bp.route('/dayof', methods=('GET',))
def dayof():
    return render_template('dayof.jinja')

@bp.route('/faq', methods=('GET',))
def faq():
    return render_template('faq.jinja')

@bp.route('/brewtour', methods=('GET',))
def brewtour():
    return render_template('brewtour.jinja')
